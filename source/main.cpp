/* main.cpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "../../multicolor/source/Multicolor.hpp"
#include "../../common/source/Common.hpp"

#include <glfw-3.1.2\include\GLFW\glfw3.h>

using namespace dynamicreflectance;
using namespace common;
using namespace multicolor;
using namespace graphicsdriver;
using namespace math;
using namespace std;

int main()
{
    int32_t glfwInitResult = glfwInit();

    if (GL_TRUE == glfwInitResult)
    {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

       // ASSERT(false == true, "DUPA!");

        GLFWwindow *pWindow = glfwCreateWindow(1920, 1080, "Multicolor Testbed", NULL, NULL);

        if (nullptr != pWindow)
        {
            glfwMakeContextCurrent(pWindow);

            Logger logger("log.txt", LoggerSeverity(true, true, true, true, true), 512, true);

            if (true == OpenGLDriverSingleton::getInstance().init())
            {
                ImageData backgroundLayerImageData;
                ImageData middleLayerImageData;
                ImageData frontLayerImageData;
                ImageData rogImageData;

                bool backgroundLayerImageLoaded = FileLoader::image("..\\..\\assets\\background.png", out(backgroundLayerImageData));
                bool middleLayerImageLoaded = FileLoader::image("..\\..\\assets\\middle.png", out(middleLayerImageData));
                bool frontLayerImageLoaded = FileLoader::image("..\\..\\assets\\front.png", out(frontLayerImageData));
                bool rogImageLoaded = FileLoader::image("..\\..\\assets\\lights.png", out(rogImageData));
           
                if (true == backgroundLayerImageLoaded && true == middleLayerImageLoaded && true == frontLayerImageLoaded && true == rogImageLoaded)
                {
                    Sampler defaultSampler;

                    Texture backgroundLayerTexture(backgroundLayerImageData, defaultSampler, false);
                    Texture middleLayerTexture(middleLayerImageData, defaultSampler, false);
                    Texture frontLayerTexture(frontLayerImageData, defaultSampler, false);
                    Texture rogTexture(rogImageData, defaultSampler, false);

                    Renderer renderer(Size2(1920, 1080), "..\\..\\..\\multicolor\\shaders\\", &logger);

                    if (true == renderer.isCreated())
                    {
                        PerspectiveCameraTransformation camera(Vector3(0.0f, 0.0f, 0.0f), math::toRadians(0.0f), common::math::toRadians(45.0f), 1.77f, Vector2(20.0f, 1.0f));
                        ModelTransformation spriteTransformation(0.0f, Vector3(0.0, 0.0f, 0.0f), Vector2(1.0f, 1.0f));

                        Scene scene;
                        float fs = 3.0f;
                        spriteTransformation.setTransformation(Vector2(1.7f * fs, 1.0f * fs), 0.0f, Vector3(0.0f, 0.0f, 3.3f));
                        scene.addSprite(SpriteMaterial(backgroundLayerTexture, Color4(1.0f, 1.0f, 1.0f, 1.0f)), spriteTransformation, 0.01f, false);
                        
                        spriteTransformation.setTransformation(Vector2(1.7f * fs, 1.0f * fs), 0.0f, Vector3(1.7f * fs, 0.0f, 3.3f));
                        scene.addSprite(SpriteMaterial(backgroundLayerTexture, Color4(1.0f, 1.0f, 1.0f, 1.0f)), spriteTransformation, 0.01f, false);
                        
                        spriteTransformation.setTransformation(Vector2(1.7f * fs, 1.0f * fs), 0.0f, Vector3(-1.7f * fs, 0.0f, 3.3f));
                        scene.addSprite(SpriteMaterial(backgroundLayerTexture, Color4(1.0f, 1.0f, 1.0f, 1.0f)), spriteTransformation, 0.01f, false);
                        
                        fs = 1.0f;//12.0f;
                        spriteTransformation.setTransformation(Vector2(1.7f * fs, 1.0f * fs), 0.0f, Vector3(0.0f, -0.5f, 2.5f));
                        scene.addSprite(SpriteMaterial(middleLayerTexture, Color4(1.0f, 1.0f, 1.0f, 1.0f)), spriteTransformation, 0.01f, true);
                        
                        spriteTransformation.setTransformation(Vector2(1.7f * fs, 1.0f * fs), 0.0f, Vector3(1.7f * fs, -0.5f, 2.5f));
                        scene.addSprite(SpriteMaterial(middleLayerTexture, Color4(1.0f, 1.0f, 1.0f, 1.0f)), spriteTransformation, 0.01f, true);
                        
                        spriteTransformation.setTransformation(Vector2(1.7f * fs, 1.0f * fs), 0.0f, Vector3(-1.7f * fs, -0.5f, 2.5f));
                        scene.addSprite(SpriteMaterial(middleLayerTexture, Color4(1.0f, 1.0f, 1.0f, 1.0f)), spriteTransformation, 0.01f, true);
                        
                        fs = 1.0f;//10.0f;
                        spriteTransformation.setTransformation(Vector2(1.7f * fs, 1.0f * fs), 0.0f, Vector3(-0.0f, 0.0f, 1.4f));
                        scene.addSprite(SpriteMaterial(frontLayerTexture, Color4(1.0f, 1.0f, 1.0f, 1.0f)), spriteTransformation, 0.01f, true);
                        
                        spriteTransformation.setTransformation(Vector2(1.7f * fs, 1.0f * fs), 0.0f, Vector3(1.7f * fs, 0.0f, 1.4f));
                        scene.addSprite(SpriteMaterial(frontLayerTexture, Color4(1.0f, 1.0f, 1.0f, 1.0f)), spriteTransformation, 0.01f, true);
                        
                        spriteTransformation.setTransformation(Vector2(1.7f * fs, 1.0f * fs), 0.0f, Vector3(-1.7f * fs, 0.0f, 1.4f));
                        scene.addSprite(SpriteMaterial(frontLayerTexture, Color4(1.0f, 1.0f, 1.0f, 1.0f)), spriteTransformation, 0.01f, true);
                        
                        fs = 1.0f;//10.0f;
                        spriteTransformation.setTransformation(Vector2(1.7f * fs, 1.0f * fs), 0.0f, Vector3(0.0f, 0.9f, 2.8f));
                        Sprite *pROG = scene.addSprite(SpriteMaterial(rogTexture, Color4(1.0f, 1.0f, 1.0f, 1.0f)), spriteTransformation, 0.01f, true);

                        const float cameraXShift = 0.0005f;

                        Vector4 v1 = normalize(Vector4(0.0f, 0.0f, -1.0f, 0.0f));

                        std::vector<DebugDrawPoint> points
                        {
                            DebugDrawPoint(Vector3(0.0f, 0.0f, 3.0f), Color4(1.0f, 1.0f, 1.0f, 0.0f), 70.0f)
                        };

                        std::vector<DebugDrawLine> lines
                        {
                            DebugDrawLine(Vector3(2.0f, 4.0f, 5.3f), Vector3(0.0f, 0.0f, 6.3f), Color4(1.0f, 0.0f, 0.0f, 0.0f), 50.0f)
                        };

                        std::vector<DebugDrawBox> boxes
                        {
                            DebugDrawBox(Vector3(0.0f, 2.0f, 4.601f), Vector2(1.0f, 1.0f), Color4(0.0f, 1.0f, 0.0f, 0.0f))
                        };

                        std::vector<DebugDrawCube> cubes
                        {
                            DebugDrawCube(Vector3(5.0f, 0.0f, 4.0f), Vector3(1.0f, 1.0f, 1.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f), 10)
                        };

                       // renderer.enable(RendererFeature::LIGHTING);

                        DirectionalLight dl1({ 0.0f, 0.0f, 1.0f }, {1.0f, 1.0f, 1.0f, 1.0f});

                        bool close = false;
                        while (false == close)
                        {
                            std::vector<Sprite> sprites = scene.getCalculateSprites(inout(camera));
                            //std::vector<Light> lightsBuffer = scene.getCalculateLights(inout(camera));

                            renderer.begin(Color4(0.0f, 0.5f, 1.0f, 0.0f));

                                renderer.getDebugDrawRenderingTechnique().setData(inout(camera), points, lines, boxes, cubes);
                                renderer.getDebugDrawRenderingTechnique().draw();

                                renderer.getFlatRenderingTechnique().setData(inout(camera), sprites);
                                renderer.getFlatRenderingTechnique().draw();
              
                            renderer.end();

                            Vector3 cameraPosition = camera.getPosition();
                            Vector3 pos = pROG->getTransformation().getPosition();
                            pos.x += cameraXShift;
                            pROG->setPosition(pos);
                            cameraPosition.x += cameraXShift;
                            camera.setPosition(cameraPosition);

                            close = 0 != glfwWindowShouldClose(pWindow);
                            glfwSwapBuffers(pWindow);
                            glfwPollEvents();
                        }
                    }
                }
            }

            OpenGLDriverSingleton::getInstance().release();
            glfwDestroyWindow(pWindow);
            glfwTerminate();
        }
    }
}